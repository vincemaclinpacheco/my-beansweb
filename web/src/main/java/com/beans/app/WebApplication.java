package com.beans.app;

import com.beans.repository.base.BaseRepositoryFactoryBean;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@ComponentScan(basePackages = {"com.beans.*"})
@EntityScan(basePackages = {"com.beans.model"})
@EnableJpaRepositories(basePackages = {"com.beans.repository"},
        repositoryFactoryBeanClass = BaseRepositoryFactoryBean.class)
@SpringBootApplication(
        scanBasePackages = {
                "com.beans.app",
                "com.beans.service",
                "com.beans.repository"
        }
)
public class WebApplication {

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class);
    }
}
