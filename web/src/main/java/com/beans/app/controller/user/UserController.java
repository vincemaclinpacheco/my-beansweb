package com.beans.app.controller.user;


import com.beans.model.base.query.QueryOperator;
import com.beans.model.base.query.QueryParameter;
import com.beans.model.base.query.QueryResult;
import com.beans.model.base.query.SortOrder;
import com.beans.model.doctor.Doctor;
import com.beans.model.user.PasswordHistory;
import com.beans.model.user.User;
import com.beans.service.user.PasswordHistoryService;
import com.beans.service.user.UserService;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RequestMapping("/users")
@RestController
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    PasswordHistoryService passwordHistoryService;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    @PostMapping()
    public QueryResult<User> create (@RequestBody User user) {
        QueryResult<User> resultSet = new QueryResult<>();

        try {
            User newUser = userService.create(user);
            resultSet.setContent(newUser);

        } catch (Exception e) {
            resultSet.setSuccess(false);
            resultSet.setErrorMessage(e.getMessage());
        }

        return resultSet;
    }

    @GetMapping(value = "/{id}")
    public QueryResult<User> retrieve(@PathVariable Long id) {
        QueryResult<User> resultSet = new QueryResult<>();

        try {
            if (id == null || id.compareTo(0L) == 0) throw new Exception("Invalid ID Parameter.");
            resultSet.setContent(userService.retrieve(id));
            resultSet.setSuccess(true);
            resultSet.setTotalPages(1);
            resultSet.setNumberOfElements(1);
            resultSet.setTotalElements(1L);
        } catch (Exception e) {
            resultSet.setSuccess(false);
            resultSet.setErrorMessage(e.getMessage());
        }

        return resultSet;
    }

    @PutMapping(value = "/{id}")
    public QueryResult<User> update(@RequestBody User toUpdate,
                                    @PathVariable("id") Long id) {
        QueryResult<User> resultSet = new QueryResult<>();

        try {
            if (id == null || id == 0L) throw new Exception("Invalid ID Parameter");
            if (toUpdate == null) throw new Exception("Invalid User Entity");
            toUpdate.setId(id);
            resultSet.setContent(userService.update(toUpdate));
            resultSet.setSuccess(true);
            resultSet.setTotalElements(1L);
            resultSet.setNumberOfElements(1);
            resultSet.setTotalPages(1);
        } catch (Exception e) {
            resultSet.setSuccess(false);
            resultSet.setErrorMessage(e.getMessage());
        }

        return resultSet;
    }

    @GetMapping(value = "/list")
    public QueryResult<User> list(@RequestParam("page") int page,
                                  @RequestParam("pageSize") int pageSize) {
        QueryResult<User> resultSet = new QueryResult<>();

        try {
            resultSet = userService.listAll(Lists.newArrayList(), page, pageSize, new SortOrder(SortOrder.Direction.DESC, "id"));
        } catch (Exception e) {
            resultSet.setErrorMessage(e.getMessage());
            resultSet.setSuccess(false);
        }

        return resultSet;
    }

    @PostMapping(value = "/query/results")
    public QueryResult<User> search(@RequestBody QueryParameter[] searchParameters,
                                    @RequestParam("page") int page,
                                    @RequestParam("pageSize") int pageSize) {
        QueryResult<User> resultSet = new QueryResult<>();

        try {
            if (searchParameters == null || searchParameters.length == 0) throw new Exception("Blank search parameters.");

            resultSet = userService.listAll(Arrays.asList(searchParameters), page, pageSize, new SortOrder(SortOrder.Direction.DESC, "id"));
        } catch (Exception e) {
            resultSet.setErrorMessage(e.getMessage());
            resultSet.setSuccess(false);
        }

        return resultSet;
    }

    @GetMapping("/display/{userId}")
    public QueryResult<User> getDoctorByName(@PathVariable String userId) {
        QueryResult<User> resultSet = new QueryResult<>();
        try {
            resultSet.setContent(userService.retrieve(new QueryParameter("userId", QueryOperator.EQ_IC, userId)));
        } catch (Exception e) {
            resultSet.setErrorMessage(e.getMessage());
            resultSet.setSuccess(Boolean.FALSE);
            e.printStackTrace();
        }
        return resultSet;
    }

    @GetMapping("/password/{id}")
    public QueryResult<PasswordHistory> getPassword(@PathVariable Long id) {
        QueryResult<PasswordHistory> resultSet = new QueryResult<>();
        try {
            resultSet.setContent(passwordHistoryService.retrieve(id));
        } catch (Exception e) {
            resultSet.setErrorMessage(e.getMessage());
            resultSet.setSuccess(Boolean.FALSE);
            e.printStackTrace();
        }
        return resultSet;
    }

    @GetMapping(value = "/encrypt")
    public QueryResult<PasswordHistory> encryptPasswords() {
        QueryResult<PasswordHistory> resultSet = new QueryResult<>();

        try {
            List<QueryParameter> qryParam = new ArrayList<>();
            QueryParameter isActiveFilter   = new QueryParameter("isActive", QueryOperator.EQ, 1);
            qryParam.add(isActiveFilter);
            List<PasswordHistory> passwordHistories = passwordHistoryService.listAll(
                    Lists.newArrayList(),
                    0, Integer.MAX_VALUE, new SortOrder(SortOrder.Direction.ASC, "id")).getContentList();
            for (PasswordHistory password : passwordHistories) {
                password.setPassword(bcryptEncoder.encode(password.getPassword()));
                passwordHistoryService.update(password);
            }
            resultSet.setContentList(passwordHistories);
        } catch (Exception e) {
            resultSet.setErrorMessage(e.getMessage());
            resultSet.setSuccess(false);
        }
        return resultSet;
    }


}
