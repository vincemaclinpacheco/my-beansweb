package com.beans.app.controller.publisher;


import com.beans.model.author.Author;
import com.beans.model.author.AuthorDto;
import com.beans.model.base.query.QueryParameter;
import com.beans.model.base.query.QueryResult;
import com.beans.model.base.query.SortOrder;
import com.beans.model.publisher.Publisher;
import com.beans.model.publisher.PublisherDto;
import com.beans.service.publisher.PublisherService;
import com.google.common.collect.Lists;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.stream.Collectors;

@RequestMapping("/publishers")
@RestController
public class PublisherController {

    @Autowired
    PublisherService publisherService;
    @Autowired
    private ModelMapper modelMapper;

    @GetMapping(value = "/{id}")
    public QueryResult<Publisher> retrieve(@PathVariable Long id) {
        QueryResult<Publisher> resultSet = new QueryResult<>();

        try {
            if (id == null || id.compareTo(0L) == 0) throw new Exception("Invalid ID Parameter.");
            resultSet.setContent(publisherService.retrieve(id));
            resultSet.setSuccess(true);
            resultSet.setTotalPages(1);
            resultSet.setNumberOfElements(1);
            resultSet.setTotalElements(1L);
        } catch (Exception e) {
            resultSet.setSuccess(false);
            resultSet.setErrorMessage(e.getMessage());
        }

        return resultSet;
    }

    @PutMapping(value = "/{id}")
    public QueryResult<Publisher> update(@RequestBody Publisher toUpdate,
                                      @PathVariable("id") Long id) {
        QueryResult<Publisher> resultSet = new QueryResult<>();

        try {
            if (id == null || id == 0L) throw new Exception("Invalid ID Parameter");
            if (toUpdate == null) throw new Exception("Invalid User Entity");
            toUpdate.setId(id);
            resultSet.setContent(publisherService.update(toUpdate));
            resultSet.setSuccess(true);
            resultSet.setTotalElements(1L);
            resultSet.setNumberOfElements(1);
            resultSet.setTotalPages(1);
        } catch (Exception e) {
            resultSet.setSuccess(false);
            resultSet.setErrorMessage(e.getMessage());
        }

        return resultSet;
    }

    @PostMapping()
    public QueryResult<Publisher> create (@RequestBody Publisher publisher) {
        QueryResult<Publisher> resultSet = new QueryResult<>();

        try {
            Publisher newPublisher = publisherService.create(publisher);
            resultSet.setContent(newPublisher);

        } catch (Exception e) {
            resultSet.setSuccess(false);
            resultSet.setErrorMessage(e.getMessage());
        }

        return resultSet;
    }

    @GetMapping(value = "/list")
    public QueryResult<Publisher> list(@RequestParam("page") int page,
                                  @RequestParam("pageSize") int pageSize) {
        QueryResult<Publisher> resultSet = new QueryResult<>();

        try {
            resultSet = publisherService.listAll(Lists.newArrayList(), page, pageSize, new SortOrder(SortOrder.Direction.DESC, "id"));
        } catch (Exception e) {
            resultSet.setErrorMessage(e.getMessage());
            resultSet.setSuccess(false);
        }

        return resultSet;
    }

    @GetMapping(value = "/list/dto")
    public QueryResult<PublisherDto> listDto(@RequestParam("page") int page,
                                             @RequestParam("pageSize") int pageSize) {
        QueryResult<PublisherDto> resultSet = new QueryResult<>();

        try {
            QueryResult<Publisher> publisherQueryResult = publisherService.listAll(Lists.newArrayList(), page, pageSize, new SortOrder(SortOrder.Direction.DESC, "id"));
            resultSet.setTotalPages(publisherQueryResult.getTotalPages());
            resultSet.setTotalElements(publisherQueryResult.getTotalElements());
            resultSet.setNumberOfElements(publisherQueryResult.getNumberOfElements());

            modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
            resultSet.setContentList(publisherQueryResult.getContentList()
                    .stream()
                    .map(publisher -> modelMapper.map(publisher, PublisherDto.class))
                    .collect(Collectors.toList())
            );
        } catch (Exception e) {
            resultSet.setErrorMessage(e.getMessage());
            resultSet.setSuccess(false);
        }

        return resultSet;
    }

    @PostMapping(value = "/query/results")
    public QueryResult<Publisher> search(@RequestBody QueryParameter[] searchParameters,
                                      @RequestParam("page") int page,
                                      @RequestParam("pageSize") int pageSize) {
        QueryResult<Publisher> resultSet = new QueryResult<>();

        try {
            if (searchParameters == null || searchParameters.length == 0) throw new Exception("Blank search parameters.");

            resultSet = publisherService.listAll(Arrays.asList(searchParameters), page, pageSize, new SortOrder(SortOrder.Direction.DESC, "id"));
        } catch (Exception e) {
            resultSet.setErrorMessage(e.getMessage());
            resultSet.setSuccess(false);
        }

        return resultSet;
    }

    @PostMapping(value = "/query/results/dto")
    public QueryResult<PublisherDto> searchDto(@RequestBody QueryParameter[] searchParameters,
                                            @RequestParam("page") int page,
                                            @RequestParam("pageSize") int pageSize) {
        QueryResult<PublisherDto> resultSet = new QueryResult<>();

        try {
            if (searchParameters == null || searchParameters.length == 0) throw new Exception("Blank search parameters.");
            QueryResult<Publisher> publisherQueryResult = publisherService.listAll(Arrays.asList(searchParameters), page, pageSize, new SortOrder(SortOrder.Direction.DESC, "id"));
            resultSet.setTotalPages(publisherQueryResult.getTotalPages());
            resultSet.setTotalElements(publisherQueryResult.getTotalElements());
            resultSet.setNumberOfElements(publisherQueryResult.getNumberOfElements());

            modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
            resultSet.setContentList(publisherQueryResult.getContentList()
                    .stream()
                    .map(publisher -> modelMapper.map(publisher, PublisherDto.class))
                    .collect(Collectors.toList())
            );
        } catch (Exception e) {
            resultSet.setErrorMessage(e.getMessage());
            resultSet.setSuccess(false);
        }

        return resultSet;
    }
}
