package com.beans.app.controller.doctor;


import com.beans.model.base.query.QueryOperator;
import com.beans.model.base.query.QueryParameter;
import com.beans.model.base.query.QueryResult;
import com.beans.model.doctor.Doctor;
import com.beans.service.doctor.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/doctors")
@RestController
public class DoctorController {

    @Autowired
    private DoctorService service;

    @GetMapping("/list")
    public List<Doctor> getDoctors() {
        return service.getDoctors();
    }

    @GetMapping("/display/{name}")
    public QueryResult<Doctor> getDoctorByName(@PathVariable String name) {
        QueryResult<Doctor> resultSet = new QueryResult<>();
        try {
            resultSet.setContent(service.retrieve(new QueryParameter("name", QueryOperator.EQ_IC, name)));
        } catch (Exception e) {
            resultSet.setErrorMessage(e.getMessage());
            resultSet.setSuccess(Boolean.FALSE);
            e.printStackTrace();
        }
        return resultSet;
    }

}
