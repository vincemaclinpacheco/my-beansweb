package com.beans.app.controller.book;


import com.beans.model.base.query.QueryParameter;
import com.beans.model.base.query.QueryResult;
import com.beans.model.base.query.SortOrder;
import com.beans.model.book.Book;
import com.beans.model.book.BookDto;
import com.beans.model.user.User;
import com.beans.service.book.BookService;
import com.google.common.collect.Lists;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequestMapping("/books")
@RestController
public class BookController {

    @Autowired
    BookService bookService;
    @Autowired
    private ModelMapper modelMapper;

    @GetMapping(value = "/{id}")
    public QueryResult<Book> retrieve(@PathVariable Long id) {
        QueryResult<Book> resultSet = new QueryResult<>();

        try {
            if (id == null || id.compareTo(0L) == 0) throw new Exception("Invalid ID Parameter.");
            resultSet.setContent(bookService.retrieve(id));
            resultSet.setSuccess(true);
            resultSet.setTotalPages(1);
            resultSet.setNumberOfElements(1);
            resultSet.setTotalElements(1L);
        } catch (Exception e) {
            resultSet.setSuccess(false);
            resultSet.setErrorMessage(e.getMessage());
        }

        return resultSet;
    }

    @PutMapping(value = "/{id}")
    public QueryResult<Book> update(@RequestBody Book toUpdate,
                                    @PathVariable("id") Long id) {
        QueryResult<Book> resultSet = new QueryResult<>();

        try {
            if (id == null || id == 0L) throw new Exception("Invalid ID Parameter");
            if (toUpdate == null) throw new Exception("Invalid User Entity");
            toUpdate.setId(id);
            resultSet.setContent(bookService.update(toUpdate));
            resultSet.setSuccess(true);
            resultSet.setTotalElements(1L);
            resultSet.setNumberOfElements(1);
            resultSet.setTotalPages(1);
        } catch (Exception e) {
            resultSet.setSuccess(false);
            resultSet.setErrorMessage(e.getMessage());
        }

        return resultSet;
    }

    @PostMapping()
    public QueryResult<Book> create (@RequestBody Book book) {
        QueryResult<Book> resultSet = new QueryResult<>();

        try {
            Book newBook = bookService.create(book);
            resultSet.setContent(newBook);

        } catch (Exception e) {
            resultSet.setSuccess(false);
            resultSet.setErrorMessage(e.getMessage());
        }

        return resultSet;
    }

    @GetMapping(value = "/list")
    public QueryResult<Book> list(@RequestParam("page") int page,
                                    @RequestParam("pageSize") int pageSize) {
        QueryResult<Book> resultSet = new QueryResult<>();

        try {
            resultSet = bookService.listAll(Lists.newArrayList(), page, pageSize, new SortOrder(SortOrder.Direction.DESC, "id"));
        } catch (Exception e) {
            resultSet.setErrorMessage(e.getMessage());
            resultSet.setSuccess(false);
        }

        return resultSet;
    }

    @PostMapping(value = "/query/results")
    public QueryResult<Book> search(@RequestBody QueryParameter[] searchParameters,
                                    @RequestParam("page") int page,
                                    @RequestParam("pageSize") int pageSize) {
        QueryResult<Book> resultSet = new QueryResult<>();

        try {
            if (searchParameters == null || searchParameters.length == 0) throw new Exception("Blank search parameters.");

            resultSet = bookService.listAll(Arrays.asList(searchParameters), page, pageSize, new SortOrder(SortOrder.Direction.DESC, "id"));
        } catch (Exception e) {
            resultSet.setErrorMessage(e.getMessage());
            resultSet.setSuccess(false);
        }

        return resultSet;
    }

    @PostMapping(value = "/query/results/dto")
    public QueryResult<BookDto> searchDto(@RequestBody QueryParameter[] searchParameters,
                                    @RequestParam("page") int page,
                                    @RequestParam("pageSize") int pageSize) {
        QueryResult<BookDto> resultSet = new QueryResult<>();

        try {
            if (searchParameters == null || searchParameters.length == 0) throw new Exception("Blank search parameters.");
            QueryResult<Book> bookQueryResult = bookService.listAll(Arrays.asList(searchParameters), page, pageSize, new SortOrder(SortOrder.Direction.DESC, "id"));
            resultSet.setTotalPages(bookQueryResult.getTotalPages());
            resultSet.setTotalElements(bookQueryResult.getTotalElements());
            resultSet.setNumberOfElements(bookQueryResult.getNumberOfElements());

            modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
            resultSet.setContentList(bookQueryResult.getContentList()
                    .stream()
                    .map(book -> modelMapper.map(book, BookDto.class))
                    .collect(Collectors.toList())
            );
        } catch (Exception e) {
            resultSet.setErrorMessage(e.getMessage());
            resultSet.setSuccess(false);
        }

        return resultSet;
    }

    @GetMapping(value = "/dto/{id}")
    public QueryResult<BookDto> retrieveDto(@PathVariable Long id) {
        QueryResult<BookDto> resultSet = new QueryResult<>();

        try {
            if (id == null || id.compareTo(0L) == 0) throw new Exception("Invalid ID Parameter.");
            modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

            resultSet.setContentList(
                    Stream.of(bookService.retrieve(id)).
                            map(book -> modelMapper.map(book, BookDto.class)).
                            collect(Collectors.toList())
            );
            resultSet.setSuccess(true);
            resultSet.setTotalPages(1);
            resultSet.setNumberOfElements(1);
            resultSet.setTotalElements(1L);
        } catch (Exception e) {
            resultSet.setSuccess(false);
            resultSet.setErrorMessage(e.getMessage());
        }

        return resultSet;
    }

    @GetMapping(value = "/list/dto")
    public QueryResult<BookDto> listDto(@RequestParam("page") int page,
                                        @RequestParam("pageSize") int pageSize) {
        QueryResult<BookDto> resultSet = new QueryResult<>();

        try {
            QueryResult<Book> bookQueryResult = bookService.listAll(Lists.newArrayList(), page, pageSize, new SortOrder(SortOrder.Direction.DESC, "id"));
            resultSet.setTotalPages(bookQueryResult.getTotalPages());
            resultSet.setTotalElements(bookQueryResult.getTotalElements());
            resultSet.setNumberOfElements(bookQueryResult.getNumberOfElements());

            modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
            resultSet.setContentList(bookQueryResult.getContentList()
                    .stream()
                    .map(book -> modelMapper.map(book, BookDto.class))
                    .collect(Collectors.toList())
            );
        } catch (Exception e) {
            resultSet.setErrorMessage(e.getMessage());
            resultSet.setSuccess(false);
        }

        return resultSet;
    }
}
