package com.beans.app.controller.auth;


import com.beans.app.config.JwtTokenUtil;
import com.beans.app.model.JwtRequest;
import com.beans.app.service.JwtUserDetailsService;
import com.beans.model.base.query.QueryOperator;
import com.beans.model.base.query.QueryParameter;
import com.beans.model.base.query.QueryResult;
import com.beans.model.user.PasswordHistory;
import com.beans.model.user.User;
import com.beans.model.user.UserSession;
import com.beans.service.user.PasswordHistoryService;
import com.beans.service.user.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/auth")
public class LoginController {

    private final Log log = LogFactory.getLog(getClass());

    @Resource
    private UserService userService;

    @Resource
    private PasswordHistoryService passwordService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    @Autowired
    private JwtUserDetailsService userDetailsService;

    @Autowired
    private AuthenticationManager authenticationManager;


    @PostMapping("/login")
    public QueryResult<Object> login(HttpServletRequest request,
                                     @RequestBody User user) {

        QueryResult<Object> resultSet = new QueryResult<>();
        try {
            User loginUser = userService.retrieve(new QueryParameter("userId", QueryOperator.EQ_IC, user.getUserId()));

            if (loginUser == null) {
                throw new Exception("Invalid username and/or password");
            }

//            if (isLocked(loginUser)) {
//                throw new Exception("Account locked. Please contact your system administrator");
//            }

            PasswordHistory activePassword = passwordService.retrieve(
                    new QueryParameter("userDbId", QueryOperator.EQ, loginUser.getId()),
                    new QueryParameter("isActive", QueryOperator.EQ, 1)
            );

            String legitPassword = activePassword.getPassword();
            String loginPassword = user.getPassword();

            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            if (!encoder.matches(user.getPassword(), activePassword.getPassword())) {
                loginUser.setLoginAttempts(loginUser.getLoginAttempts() + 1); //increment login attempt
                userService.update(loginUser);
                throw new Exception("Invalid username and/or password");
            }

            if (isExpired(activePassword)) {
                List<Object> resultList = new ArrayList<>();
                resultList.add(loginUser);
                resultList.add(user.getPassword());
                resultSet.setSuccess(false);
                resultSet.setContent(resultList);
                resultSet.setErrorMessage("expired");
                return resultSet;
            }  else if (isTemporary(activePassword)) {
                List<Object> resultList = new ArrayList<>();
                resultList.add(loginUser);
                resultList.add(user.getPassword());
                resultSet.setSuccess(false);
                resultSet.setContent(resultList);
                resultSet.setErrorMessage("temporary");
                return resultSet;
            } else {
                authenticate(user.getUserId(), user.getPassword());
                // load UserDetails object of user
                final UserDetails userDetails = userDetailsService
                        .loadUserByUsername(user.getUserId());

                // generate a valid token
                final String token = jwtTokenUtil.generateToken(userDetails);

                //clear login attempts
                loginUser.setLoginAttempts(0);
                //set login date
                loginUser.setLastLoginDate(LocalDateTime.now());
                userService.update(loginUser);

                UserSession userSession = new UserSession();
                userSession.setUser(loginUser);
                userSession.setToken(token);

                resultSet.setSuccess(true);
                resultSet.setTotalElements(1L);
                resultSet.setTotalPages(1);
                resultSet.setContent(userSession);

                if (log.isInfoEnabled()) {
                    log.info("User logged in successfully...");
                }
            }


        } catch (Exception e) {
            resultSet.setErrorMessage(e.getMessage());
            resultSet.setSuccess(false);
        }
        return resultSet;
    }

    @PostMapping(value="/login/change_password")
    public QueryResult<Object> updatePassword(HttpServletRequest request,
                                              @RequestBody User user){
        QueryResult<Object> resultSet = new QueryResult<>();

        try {
            User loginUser = userService.retrieve(new QueryParameter("userId", QueryOperator.EQ_IC, user.getUserId()));
            String newPassword = user.getNewPassword();

            if (passwordService.updatePassword(loginUser, newPassword)) {
                resultSet.setSuccess(true);
            } else {
                resultSet.setSuccess(false);
            }
        } catch (Exception e) {
            resultSet.setSuccess(false);
            resultSet.setErrorMessage(e.getMessage());
        }

        return resultSet;
    }

    private boolean isExpired(PasswordHistory activePassword) {
        LocalDate currentDate = LocalDate.now();
        boolean isExpired = false;

        if (activePassword.getExpiryDate().compareTo(currentDate) <= 0) {
            isExpired = true;
        } else {
            isExpired = false;
        }

        return isExpired;
//		return (activePassword.getExpiryDate().compareTo(new Date(LocalDate.now().toEpochDay())) <= 0);
    }

    private boolean isTemporary(PasswordHistory activePassword) {
        return (activePassword.getExpiryDate().compareTo(activePassword.getCreateDate()) == 0);
    }

    private boolean isLocked(User user) {
        return (user.getLoginAttempts() >= 5);
    }


    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

}
