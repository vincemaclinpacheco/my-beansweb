package com.beans.app.controller.author;


import com.beans.model.author.Author;
import com.beans.model.author.AuthorDto;
import com.beans.model.base.query.QueryParameter;
import com.beans.model.base.query.QueryResult;
import com.beans.model.base.query.SortOrder;
import com.beans.model.book.Book;
import com.beans.model.book.BookDto;
import com.beans.model.publisher.Publisher;
import com.beans.model.publisher.PublisherDto;
import com.beans.model.user.User;
import com.beans.service.author.AuthorService;
import com.google.common.collect.Lists;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.stream.Collectors;

@RequestMapping("/authors")
@RestController
public class AuthorController {

    @Autowired
    AuthorService authorService;
    @Autowired
    private ModelMapper modelMapper;

    @GetMapping(value = "/{id}")
    public QueryResult<Author> retrieve(@PathVariable Long id) {
        QueryResult<Author> resultSet = new QueryResult<>();

        try {
            if (id == null || id.compareTo(0L) == 0) throw new Exception("Invalid ID Parameter.");
            resultSet.setContent(authorService.retrieve(id));
            resultSet.setSuccess(true);
            resultSet.setTotalPages(1);
            resultSet.setNumberOfElements(1);
            resultSet.setTotalElements(1L);
        } catch (Exception e) {
            resultSet.setSuccess(false);
            resultSet.setErrorMessage(e.getMessage());
        }

        return resultSet;
    }

    @PutMapping(value = "/{id}")
    public QueryResult<Author> update(@RequestBody Author toUpdate,
                                    @PathVariable("id") Long id) {
        QueryResult<Author> resultSet = new QueryResult<>();

        try {
            if (id == null || id == 0L) throw new Exception("Invalid ID Parameter");
            if (toUpdate == null) throw new Exception("Invalid User Entity");
            toUpdate.setId(id);
            resultSet.setContent(authorService.update(toUpdate));
            resultSet.setSuccess(true);
            resultSet.setTotalElements(1L);
            resultSet.setNumberOfElements(1);
            resultSet.setTotalPages(1);
        } catch (Exception e) {
            resultSet.setSuccess(false);
            resultSet.setErrorMessage(e.getMessage());
        }

        return resultSet;
    }

    @PostMapping()
    public QueryResult<Author> create (@RequestBody Author author) {
        QueryResult<Author> resultSet = new QueryResult<>();

        try {
            Author newAuthor = authorService.create(author);
            resultSet.setContent(newAuthor);

        } catch (Exception e) {
            resultSet.setSuccess(false);
            resultSet.setErrorMessage(e.getMessage());
        }

        return resultSet;
    }

    @GetMapping(value = "/list")
    public QueryResult<Author> list(@RequestParam("page") int page,
                                  @RequestParam("pageSize") int pageSize) {
        QueryResult<Author> resultSet = new QueryResult<>();

        try {
            resultSet = authorService.listAll(Lists.newArrayList(), page, pageSize, new SortOrder(SortOrder.Direction.DESC, "id"));
        } catch (Exception e) {
            resultSet.setErrorMessage(e.getMessage());
            resultSet.setSuccess(false);
        }

        return resultSet;
    }

    @GetMapping(value = "/list/dto")
    public QueryResult<AuthorDto> listDto(@RequestParam("page") int page,
                                          @RequestParam("pageSize") int pageSize) {
        QueryResult<AuthorDto> resultSet = new QueryResult<>();

        try {
            QueryResult<Author> authorQueryResult = authorService.listAll(Lists.newArrayList(), page, pageSize, new SortOrder(SortOrder.Direction.DESC, "id"));
            resultSet.setTotalPages(authorQueryResult.getTotalPages());
            resultSet.setTotalElements(authorQueryResult.getTotalElements());
            resultSet.setNumberOfElements(authorQueryResult.getNumberOfElements());

            modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
            resultSet.setContentList(authorQueryResult.getContentList()
                    .stream()
                    .map(author -> modelMapper.map(author, AuthorDto.class))
                    .collect(Collectors.toList())
            );
        } catch (Exception e) {
            resultSet.setErrorMessage(e.getMessage());
            resultSet.setSuccess(false);
        }

        return resultSet;
    }

    @PostMapping(value = "/query/results")
    public QueryResult<Author> search(@RequestBody QueryParameter[] searchParameters,
                                    @RequestParam("page") int page,
                                    @RequestParam("pageSize") int pageSize) {
        QueryResult<Author> resultSet = new QueryResult<>();

        try {
            if (searchParameters == null || searchParameters.length == 0) throw new Exception("Blank search parameters.");

            resultSet = authorService.listAll(Arrays.asList(searchParameters), page, pageSize, new SortOrder(SortOrder.Direction.DESC, "id"));
        } catch (Exception e) {
            resultSet.setErrorMessage(e.getMessage());
            resultSet.setSuccess(false);
        }

        return resultSet;
    }

    @PostMapping(value = "/query/results/dto")
    public QueryResult<AuthorDto> searchDto(@RequestBody QueryParameter[] searchParameters,
                                          @RequestParam("page") int page,
                                          @RequestParam("pageSize") int pageSize) {
        QueryResult<AuthorDto> resultSet = new QueryResult<>();

        try {
            if (searchParameters == null || searchParameters.length == 0) throw new Exception("Blank search parameters.");
            QueryResult<Author> authorQueryResult = authorService.listAll(Arrays.asList(searchParameters), page, pageSize, new SortOrder(SortOrder.Direction.DESC, "id"));
            resultSet.setTotalPages(authorQueryResult.getTotalPages());
            resultSet.setTotalElements(authorQueryResult.getTotalElements());
            resultSet.setNumberOfElements(authorQueryResult.getNumberOfElements());

            modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
            resultSet.setContentList(authorQueryResult.getContentList()
                    .stream()
                    .map(author -> modelMapper.map(author, AuthorDto.class))
                    .collect(Collectors.toList())
            );
        } catch (Exception e) {
            resultSet.setErrorMessage(e.getMessage());
            resultSet.setSuccess(false);
        }

        return resultSet;
    }
}
