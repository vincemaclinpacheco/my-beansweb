--INSERT INTO TBL_EMPLOYEES (first_name, last_name, email) VALUES
--  ('Lokesh', 'Gupta', 'abc@gmail.com'),
--  ('Deja', 'Vu', 'xyz@email.com'),
--  ('Caption', 'America', 'cap@marvel.com');
--
--
--INSERT INTO t_beans_user (user_id, first_name, last_name, email, role) VALUES
--  ('USR00', 'Vince', 'Pacheco', 'vincemaclinpacheco@gmail.com', 'admin'),
--  ('USR01', 'Tummy', 'Dummy', 'tummydummy@gmail.com', 'user'),
--  ('USR02', 'Simp', 'Pimp', 'simppimp@gmail.com', 'user');
--
--
--INSERT INTO t_beans_password_history (user_db_id, password) VALUES
--  (1, 'admin'),
--  (2, 'admin'),
--  (3, 'admin');

INSERT INTO t_beans_author (first_name, last_name, email) VALUES
    ('John', 'Green', 'johngreen@gmail.com'),
    ('JK', 'Rowling', 'jkrowling@gmail.com'),
    ('Stephen', 'King', 'stephenking@gmail.com');

INSERT INTO t_beans_publisher (name, city, country, email, telephone) VALUES
    ('Penguin Group', 'NYC', 'US', 'librariansden@us.penguingroup.com', '0000000'),
    ('Bloomsbury Publishing', 'London', 'UK', 'contact@bloomsbury.com', '1111111'),
    ('Viking Press', 'NYC', 'US', 'consumerservices@penguinrandomhouse.com', '2222222'),
    ('Double Day', 'NYC', 'US', 'ddaypub@randomhouse.com', '3333333');

INSERT INTO t_beans_book (title, format, price, publish_date, author_db_id, publisher_db_id) VALUES
    ('Paper Towns', 'paperback', 9.99, '2008-10-16', 1, 1),
    ('The Fault In Our Stars', 'paperback', 11.99, '2012-01-10', 1, 1),
    ('Harry Potter and the Sorcerers Stone', 'hardback', 15.99, '1997-06-26', 2, 2),
    ('Harry Potter and the Chamber of Secrets', 'hardback', 15.99, '1998-07-02', 2, 2),
    ('Harry Potter and the Prisoner of Azkaban', 'hardback', 15.99, '1999-07-08', 2, 2),
    ('Harry Potter and the Goblet of Fire', 'hardback', 15.99, '2000-07-08', 2, 2),
    ('Harry Potter and the Order of the Phoenix', 'hardback', 15.99, '2003-06-21', 2, 2),
    ('Harry Potter and the Half-Blood Prince', 'hardback', 15.99, '2005-07-16', 2, 2),
    ('Harry Potter and the Deathly Hallows', 'hardback', 15.99, '2007-07-21', 2, 2),
    ('Harry Potter and the Cursed Child', 'hardback', 15.99, '2016-07-31', 2, 2),
    ('It', 'paperback', 12.99, '1986-09-15', 3, 3),
    ('The Shining', 'paperback', 12.99, '1977-01-28', 3, 4);