--DROP TABLE IF EXISTS TBL_EMPLOYEES;
--
--CREATE TABLE TBL_EMPLOYEES (
--  id INT AUTO_INCREMENT  PRIMARY KEY,
--  first_name VARCHAR(250) NOT NULL,
--  last_name VARCHAR(250) NOT NULL,
--  email VARCHAR(250) DEFAULT NULL
--);
--
--DROP TABLE IF EXISTS t_beans_user;
--
--CREATE TABLE t_beans_user (
--  db_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
--  user_id VARCHAR(12) NOT NULL,
--  first_name VARCHAR(250) NOT NULL,
--  last_name VARCHAR(250) NOT NULL,
--  email VARCHAR(250) DEFAULT NULL,
--  role VARCHAR(250) DEFAULT NULL,
--  last_login_date datetime DEFAULT NULL,
--  login_attempts INT(2) DEFAULT '0'
--);
--
--DROP TABLE IF EXISTS t_beans_password_history;
--CREATE TABLE t_beans_password_history (
--    db_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
--    user_db_id INT(11) NOT NULL,
--    password VARCHAR(250) NOT NULL,
--    create_date date DEFAULT '2020-01-01',
--    expiry_date date DEFAULT '2020-01-01',
--    is_active INT(1) DEFAULT '1'
--);

DROP TABLE IF EXISTS t_beans_author;
CREATE TABLE t_beans_author (
    db_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(250) NOT NULL,
    last_name VARCHAR(250) NOT NULL,
    email VARCHAR(250) DEFAULT NULL
);

DROP TABLE IF EXISTS t_beans_publisher;
CREATE TABLE t_beans_publisher (
    db_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(250) NOT NULL,
    city VARCHAR(250) DEFAULT NULL,
    country VARCHAR(250) DEFAULT NULL,
    email VARCHAR(250) DEFAULT NULL,
    telephone VARCHAR(250) DEFAULT NULL
);

DROP TABLE IF EXISTS t_beans_book;
CREATE TABLE t_beans_book (
    db_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(250) NOT NULL,
    format VARCHAR(250) NOT NULL,
    price DECIMAL(5,2) DEFAULT NULL,
    publish_date date DEFAULT '1900-01-01',
    author_db_id INT(11) NOT NULL,
    publisher_db_id INT(11) NOT NULL
);