package com.beans.model.user;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QPasswordHistory is a Querydsl query type for PasswordHistory
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPasswordHistory extends EntityPathBase<PasswordHistory> {

    private static final long serialVersionUID = -1089841229L;

    public static final QPasswordHistory passwordHistory = new QPasswordHistory("passwordHistory");

    public final com.beans.model.base.QBaseModel _super = new com.beans.model.base.QBaseModel(this);

    public final DatePath<java.time.LocalDate> createDate = createDate("createDate", java.time.LocalDate.class);

    public final DatePath<java.time.LocalDate> expiryDate = createDate("expiryDate", java.time.LocalDate.class);

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final NumberPath<Integer> isActive = createNumber("isActive", Integer.class);

    public final StringPath password = createString("password");

    public final NumberPath<Long> userDbId = createNumber("userDbId", Long.class);

    public QPasswordHistory(String variable) {
        super(PasswordHistory.class, forVariable(variable));
    }

    public QPasswordHistory(Path<? extends PasswordHistory> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPasswordHistory(PathMetadata metadata) {
        super(PasswordHistory.class, metadata);
    }

}

