package com.beans.model.author;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QAuthor is a Querydsl query type for Author
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAuthor extends EntityPathBase<Author> {

    private static final long serialVersionUID = 1116623761L;

    public static final QAuthor author = new QAuthor("author");

    public final com.beans.model.base.QBaseModel _super = new com.beans.model.base.QBaseModel(this);

    public final SetPath<com.beans.model.book.Book, com.beans.model.book.QBook> bookSet = this.<com.beans.model.book.Book, com.beans.model.book.QBook>createSet("bookSet", com.beans.model.book.Book.class, com.beans.model.book.QBook.class, PathInits.DIRECT2);

    public final StringPath email = createString("email");

    public final StringPath firstName = createString("firstName");

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final StringPath lastName = createString("lastName");

    public QAuthor(String variable) {
        super(Author.class, forVariable(variable));
    }

    public QAuthor(Path<? extends Author> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAuthor(PathMetadata metadata) {
        super(Author.class, metadata);
    }

}

