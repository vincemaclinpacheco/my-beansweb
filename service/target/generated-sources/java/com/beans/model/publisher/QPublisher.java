package com.beans.model.publisher;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QPublisher is a Querydsl query type for Publisher
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPublisher extends EntityPathBase<Publisher> {

    private static final long serialVersionUID = 1986005581L;

    public static final QPublisher publisher = new QPublisher("publisher");

    public final com.beans.model.base.QBaseModel _super = new com.beans.model.base.QBaseModel(this);

    public final SetPath<com.beans.model.book.Book, com.beans.model.book.QBook> bookSet = this.<com.beans.model.book.Book, com.beans.model.book.QBook>createSet("bookSet", com.beans.model.book.Book.class, com.beans.model.book.QBook.class, PathInits.DIRECT2);

    public final StringPath city = createString("city");

    public final StringPath country = createString("country");

    public final StringPath email = createString("email");

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final StringPath name = createString("name");

    public final StringPath telephone = createString("telephone");

    public QPublisher(String variable) {
        super(Publisher.class, forVariable(variable));
    }

    public QPublisher(Path<? extends Publisher> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPublisher(PathMetadata metadata) {
        super(Publisher.class, metadata);
    }

}

