package com.beans.repository.author;

import com.beans.model.author.Author;
import com.beans.repository.base.BaseRepository;

public interface AuthorRepository extends BaseRepository<Author, Long> {
}
