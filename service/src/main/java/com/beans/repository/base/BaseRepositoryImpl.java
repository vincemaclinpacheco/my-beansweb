package com.beans.repository.base;


import com.beans.model.base.query.QueryOperator;
import com.beans.model.base.query.QueryParameter;
import com.beans.model.base.query.QueryResult;
import com.beans.model.base.query.SortOrder;
import com.google.common.collect.Iterables;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.*;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.annotations.QueryHints;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.querydsl.EntityPathResolver;
import org.springframework.data.querydsl.SimpleEntityPathResolver;
import org.springframework.util.StringUtils;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

public class BaseRepositoryImpl<Entity, ID extends Serializable>
        extends SimpleJpaRepository<Entity, ID> implements BaseRepository<Entity, ID> {
    private static final Log baseRepoLogger = LogFactory.getLog(BaseRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    private EntityPath<Entity> entityPath; // JPA reference to a database table via an Entity
    private PathBuilder<Entity> pathBuilder; // Querydsl reference to a database table via an Entity
    private Querydsl querydsl; // for executing Querydsl supported operations

    private Class<Entity> entityClass; // class reference to the current Entity
    private String entityName; // name of the current Entity
    protected JPAQuery<Entity> query; // query object for executing JPA supported operations

    // predefined default values
    private static final EntityPathResolver DFLT_ENPATH_RESOLVER = SimpleEntityPathResolver.INSTANCE;
    private static final int DFLT_MAX_PAGE_SIZE = Integer.MAX_VALUE;
    private static final int DFLT_PAGE_START = 0;
    private static final int DFLT_PAGE_SIZE = 50;
    private static final String DFLT_SORT_COL = "id";

    // create the patterns for comparison
    // YYYY-MM-dd
    private static final Pattern datePattern = Pattern.compile("^2[0-9]{3}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[01])$");
    // YYYY-MM-dd HH:mm:ss 24 HR format
    private static final Pattern dateTimePattern = Pattern.compile("^2[0-9]{3}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[01])T([01]?[0-9]|2[0-3])(:[0-5][0-9]){2}$");
    // Integer Value
    private static final Pattern integerPattern = Pattern.compile("^[0-9]+$");
    // Double value
    private static final Pattern doublePattern = Pattern.compile("^[0-9]+\\.[0-9]+$");

    public BaseRepositoryImpl(JpaEntityInformation<Entity, ID> entityMetadata, EntityManager entityManager) {
        this(entityMetadata, entityManager, DFLT_ENPATH_RESOLVER);
    }

    public BaseRepositoryImpl(JpaEntityInformation<Entity, ID> entityMetadata, EntityManager entityManager, EntityPathResolver resolver) {
        super(entityMetadata, entityManager);
        this.entityManager = entityManager;
        this.entityPath = resolver.createPath(entityMetadata.getJavaType());
        this.pathBuilder = new PathBuilder<>(entityPath.getType(), entityPath.getMetadata());
        this.querydsl = new Querydsl(entityManager, pathBuilder);

        baseRepoLogger.info("Instantiating " + entityMetadata.getEntityName());
        String name = entityMetadata.getEntityName();
        entityName = name.substring(0, 1).toLowerCase() + name.substring(1);
        entityClass = entityMetadata.getJavaType();
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Entity retrieve(ID id) {
        return retrieve(new QueryParameter("id", QueryOperator.EQ, id));
    }

    @Override
    public Entity retrieve(QueryParameter... parameters) {
        return retrieve(null, parameters);
    }

    @Override
    public Entity retrieve(EntityGraph<Entity> graph, QueryParameter... parameters) {
        BooleanBuilder whereClause = buildWhereClause(Arrays.asList(parameters)); // prepare the where clause
        if (query == null) query = new JPAQuery<>(entityManager); // initialize the query object
        if (graph != null) query.setHint(QueryHints.LOADGRAPH, graph); // use the provided entityGraph if it exists
        Entity result = query.from(entityPath).where(whereClause).fetchOne(); // select * from entityPath where whereClause limit 1
        query = null; // reset the query object

        return result;
    }

    @Override
    public boolean exists(QueryParameter... parameters) {
        return exists(null, parameters);
    }

    @Override
    public boolean exists(EntityGraph<Entity> graph, QueryParameter... parameters) {
        if (parameters == null || parameters.length == 0) return false; // return false if parameters do not exist

        BooleanBuilder whereClause = buildWhereClause(Arrays.asList(parameters)); // prepare the where clause

        if (query == null) query = new JPAQuery<>(entityManager); // initialize the query object

        if (graph != null) query.setHint(QueryHints.LOADGRAPH, graph); // use the provided entityGraph if it exists

        boolean flag = query.from(entityPath).where(whereClause).fetchFirst() != null; // select * from entityPath where whereClause limit 1 != null
        query = null; // reset the query object

        return flag;
    }


    @Override
    public long count(QueryParameter... parameters) {
        return count(null, parameters);
    }

    @Override
    public long count(EntityGraph<Entity> graph, QueryParameter... parameters) {
        if (query == null) query = new JPAQuery<>(entityManager); // initialize the query object

        if (graph != null) query.setHint(QueryHints.LOADGRAPH, graph); // use the provided entityGraph if it exists

        long count = query.from(entityPath).where(buildWhereClause(Arrays.asList(parameters))).fetchCount(); // select count(*) from entityPath where whereClause
        query = null; // reset the query object

        return count;
    }

    @Override
    public void delete(ID id) {
        deleteById(id); // use JPA deleteById function
    }

    @Override
    public void delete(QueryParameter... parameters) {
        delete(retrieve(parameters)); // use JPA delete(Entity) function
    }

    @Override
    public QueryResult<Entity> listAll() {
        return listAll(new ArrayList<>(), new SortOrder(SortOrder.DFLT_DIRECTION, DFLT_SORT_COL));
    }

    @Override
    public QueryResult<Entity> listAll(List<QueryParameter> parameterList, SortOrder... sortOrders) {
        return listAll(parameterList, DFLT_PAGE_START, DFLT_PAGE_SIZE, sortOrders);
    }

    @Override
    public QueryResult<Entity> listAll(List<QueryParameter> parameterList, int pageStart, int pageSize, SortOrder... sortOrders) {
        return listAll(null, parameterList, pageStart, pageSize, sortOrders);
    }

    /***
     * main listAll implementation.
     * all other implementations refer to this one
     *
     * all paginated results will start at page 0
     * pageSize cannot be less than 1
     * ***/
    @Override
    public QueryResult<Entity> listAll(EntityGraph<Entity> graph, List<QueryParameter> parameterList, int pageStart, int pageSize, SortOrder... sortOrders) {
        // check if valid values for pageSize and pageStart are used
        pageSize = pageSize < 1 ? DFLT_PAGE_SIZE : pageSize;
        pageStart = pageStart < 0 ? DFLT_PAGE_START : pageStart;

        List <Sort.Order> orderList = new ArrayList<>(); // prepare list for sort order objects
        if (sortOrders == null || Arrays.asList(sortOrders).isEmpty()) {
            // add the default sort ( order by id asc ) if there are no sort objects provided
            orderList.add(new Sort.Order(Sort.Direction.ASC, DFLT_SORT_COL));
        } else {
            // else, add the provided sort order objects in the list
            for (SortOrder order : sortOrders) {
                orderList.add(new Sort.Order(Sort.Direction.valueOf(order.getDirection().getValue()), order.getProperty()));
            }
        }

        // create a Pageable instance that will handle the pagination of results
        Pageable pageRequest = PageRequest.of(pageStart, pageSize, Sort.by(orderList));
        // initialize the result variable
        QueryResult<Entity> resultSet = new QueryResult<>();
        // create a Page instance that will hold the paginated results
        Page<Entity> entities = null;

        if (parameterList == null || parameterList.isEmpty()) {
            // if there are no parameters, just get a paginated result set
            entities = findAll(pageRequest);
        } else {
            BooleanBuilder whereClause = buildWhereClause(parameterList); // build the Predicate entity that serves as the where clause
            if (query == null) query = new JPAQuery<>(entityManager); // initialize the query object
            if (graph != null) query.setHint(QueryHints.LOADGRAPH, graph); // add the loaded EntityGraph if it exists

            // select * from entityPath where whereClause
            // entityPath will refer to the current Class being queried
            query.from(entityPath).where(whereClause);

            JPAQuery<Entity> countQuery = new JPAQuery<>(entityManager); // create a query entity for counting
            long count = countQuery.from(entityPath).where(whereClause).fetchCount(); // retrieve the count of entities covered by the whereClause
            if (count >= pageRequest.getOffset()) {
                // if count >= total elements per page, apply pagination
                entities = new PageImpl<>(querydsl.applyPagination(pageRequest, query).fetch(), pageRequest, count);
            } else {
                // else, add all entities in one page
                entities = new PageImpl<>(query.fetch(), pageRequest, count);
            }
        }

        // initialize the values of the result variable
        resultSet.setSuccess(entities.hasContent());
        resultSet.setContentList(entities.getContent());
        resultSet.setTotalElements(entities.getTotalElements());
        resultSet.setNumberOfElements(entities.getNumberOfElements());
        resultSet.setTotalPages(entities.getTotalPages());

        // reset the query object
        query = null;

        return resultSet;
    }

    @Override
    public Optional<Entity> findOne(Predicate predicate) {
        if (query == null) query = new JPAQuery<>(entityManager); // initialize the query object

        Entity resultItem = query.from(entityPath).where(predicate).fetchFirst(); // select * from entityPath where predicate limit 1
        query = null; // reset the query object
        return Optional.of(resultItem);
    }

    @Override
    public Iterable<Entity> findAll(Predicate predicate) {
        return findAll(predicate, new Sort(Sort.Direction.ASC, DFLT_SORT_COL));
    }

    @Override
    public Iterable<Entity> findAll(Predicate predicate, Sort sort) {
        List <OrderSpecifier<?>> orderSpecifierList = new ArrayList<>(); // prepare list of OrderSpecifier objects
        for (Sort.Order order : sort) {
            // convert the provided Sort.Order objects to OrderSpecifier
            orderSpecifierList.add(new OrderSpecifier (com.querydsl.core.types.Order.valueOf(order.getDirection().name()), entityPath));
        }

        return findAll(predicate, Iterables.toArray(orderSpecifierList, OrderSpecifier.class));
    }

    @Override
    public Iterable<Entity> findAll(Predicate predicate, OrderSpecifier<?>... orders) {
        if (query == null) query = new JPAQuery<>(entityManager); // initialize the query object

        Iterable<Entity> resultSet = query.from(entityPath).where(predicate).orderBy(orders).fetch(); // select * from entityPath where predicate order by orders
        query = null; // reset query object

        return resultSet;
    }

    @Override
    public Iterable<Entity> findAll(OrderSpecifier<?>... orders) {
        if (query == null) query = new JPAQuery<>(entityManager); // initialize the query object

        Iterable <Entity> resultSet = query.from(entityPath).orderBy(orders).fetch(); // select * from entityPath order by orders
        query = null; // reset query object

        return resultSet;
    }

    @Override
    public Page<Entity> findAll(Predicate predicate, Pageable pageable) {
        if (query == null) query = new JPAQuery<>(entityManager); // initialize the query object

        // this will return a paginated entity from the result of the query
        Page<Entity> resultPage = new PageImpl<>(query.from(entityPath).where(predicate).fetch(), pageable, count(predicate));
        query = null; // reset the query object

        return resultPage;
    }

    @Override
    public long count(Predicate predicate) {
        if (query == null) query = new JPAQuery<>(entityManager); // initialize the query object

        long count = query.from(entityPath).where(predicate).fetchCount(); // select count(*) from entityPath where predicate
        query = null; // reset the query object

        return count;
    }

    @Override
    public boolean exists(Predicate predicate) {
        if (query == null) query = new JPAQuery<>(entityManager); // initialize the query object
        boolean flag = query.from(entityPath).where(predicate).fetchFirst() != null; // select * from entityPath where predicate limit 1 != null
        query = null; // reset the query object

        return flag;
    }


    /***
     * creates a boolean builder to be used in creating queries from QueryParameter classes
     * converted into a function since most classes use this implementation in creating their own parameter lists
     * ***/
    private BooleanBuilder buildWhereClause (List<QueryParameter> parameterList) {
        // create a boolean builder instance
        BooleanBuilder whereClause = new BooleanBuilder();
        // iterate through the list
        for (QueryParameter parameter : parameterList) {
            Ops qOp = parameter.getqOperator().getOperand(); // retrieve the Operator to use
            Path<Entity> entityPath = Expressions.path(entityClass, entityName); // derive the entity path
            Path<String> propertyPath = Expressions.path(String.class, entityPath, parameter.getProperty()); // derive the property path
            if (!parameter.hasOrOperands()) {
                // if current parameter has no OR operands, immediately add to the boolean builder
                whereClause.and(Expressions.predicate(qOp, propertyPath, Expressions.constant(getParameterValue(parameter.getValue()))));
            } else {
                // create clause for OR
                BooleanBuilder orClause = new BooleanBuilder();
                // immediately add the current parameter to the list
                orClause.or(Expressions.predicate(qOp, propertyPath, Expressions.constant(getParameterValue(parameter.getValue()))));
                // iterate through the OR Operands and add them to the clause
                for (QueryParameter orParameter : parameter.getOrOperands()) {
                    Ops orOp = orParameter.getqOperator().getOperand();
                    Path<Entity> orEnPath = Expressions.path(entityClass, entityName);
                    Path<String> orPropPath = Expressions.path(String.class, orEnPath, orParameter.getProperty());
                    orClause.or(Expressions.predicate(orOp, orPropPath, Expressions.constant(getParameterValue(orParameter.getValue()))));
                }
                // add the bundle to the current clause
                whereClause.and(orClause);
            }
        }
        return whereClause;
    }

    /***
     * derives the data type of the parameter based on how it is presented
     * all data is received as string and are converted to their respective types using REGEX matching and proper parsing
     * ***/
    private Object getParameterValue(Object value) {
        try {
            if (value instanceof String) {
//                String currValue = StringUtils.trimLeadingWhitespace(StringUtils.trimTrailingWhitespace(value.toString()));
                String currValue = value.toString();
                if (datePattern.matcher(currValue).matches()) {
                    return LocalDate.parse(currValue, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                } else if (dateTimePattern.matcher(currValue).matches()) {
                    return LocalDateTime.parse(currValue.replaceAll("T"," "), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                } else if (integerPattern.matcher(currValue).matches() && Integer.parseInt(currValue) > 0) {
                    return Integer.parseInt(currValue);
                } else if (doublePattern.matcher(currValue).matches() && Double.parseDouble(currValue) > 0) {
                    return Double.parseDouble(currValue);
                } else if (currValue.contains("*")) {
                    return currValue.replaceAll("\\*", "%");
                }
            }
            return value;
        } catch (Exception e) {
            return value;
        }
    }
}
