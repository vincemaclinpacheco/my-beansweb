package com.beans.repository.base;

import com.beans.model.base.query.QueryParameter;
import com.beans.model.base.query.QueryResult;
import com.beans.model.base.query.SortOrder;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import javax.persistence.EntityGraph;
import java.io.Serializable;
import java.util.List;

@NoRepositoryBean
public interface BaseRepository <Entity, ID extends Serializable> extends CrudRepository<Entity, ID>, QuerydslPredicateExecutor<Entity> {
    Entity retrieve(ID id);
    Entity retrieve(QueryParameter... parameters);
    Entity retrieve(EntityGraph<Entity> graph, QueryParameter... parameters);

    boolean exists(QueryParameter ... parameters);
    boolean exists(EntityGraph<Entity> graph, QueryParameter ... parameters);

    long count(QueryParameter ... parameters);
    long count(EntityGraph<Entity> graph, QueryParameter ... parameters);

    void delete(ID id);
    void delete(QueryParameter ... parameters);

    // the following are paginated functions
    QueryResult<Entity> listAll();
    QueryResult<Entity> listAll(List<QueryParameter> parameterList, SortOrder... sortOrders);
    QueryResult<Entity> listAll(List<QueryParameter> parameterList, int pageStart, int pageSize, SortOrder ... sortOrders);
    QueryResult<Entity> listAll(EntityGraph<Entity> graph, List<QueryParameter> parameterList, int pageStart, int pageSize, SortOrder ... sortOrders);

}
