package com.beans.repository.publisher;

import com.beans.model.publisher.Publisher;
import com.beans.repository.base.BaseRepository;

public interface PublisherRepository extends BaseRepository<Publisher, Long> {
}
