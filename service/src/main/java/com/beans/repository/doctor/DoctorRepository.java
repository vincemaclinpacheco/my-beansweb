package com.beans.repository.doctor;

import com.beans.model.doctor.Doctor;
import com.beans.repository.base.BaseRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DoctorRepository extends BaseRepository<Doctor, Long> {
}
