package com.beans.repository.user;

import com.beans.model.user.PasswordHistory;
import com.beans.repository.base.BaseRepository;

public interface PasswordHistoryRepository extends BaseRepository<PasswordHistory, Long> {
}
