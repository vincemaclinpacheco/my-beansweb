package com.beans.repository.user;

import com.beans.model.user.User;
import com.beans.repository.base.BaseRepository;

public interface UserRepository extends BaseRepository<User, Long> {
}
