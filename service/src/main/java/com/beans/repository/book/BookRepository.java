package com.beans.repository.book;

import com.beans.model.book.Book;
import com.beans.repository.base.BaseRepository;

public interface BookRepository extends BaseRepository<Book, Long> {
}
