package com.beans.model.author;

import com.beans.model.base.BaseModel;

public class AuthorDto extends BaseModel {

    private static final long serialVersionUID = -3912567069854461307L;

    private String          firstName;
    private String          lastName;
    private String          email;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
