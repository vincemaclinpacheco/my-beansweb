package com.beans.model.user;


import com.beans.model.base.BaseModel;
import com.google.common.base.MoreObjects;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "t_beans_password_history")
public class PasswordHistory extends BaseModel {

    private String              password;
    private LocalDate           createDate;
    private LocalDate           expiryDate;
    private Integer             isActive;

    private Long             userDbId;

    public PasswordHistory(){}

    @Column(name = "user_db_id")
    public Long getUserDbId() {
        return userDbId;
    }

    public void setUserDbId(Long userDbId) {
        this.userDbId = userDbId;
    }

    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "create_date")
    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    @Column(name = "expiry_date")
    public LocalDate getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDate expiryDate) {
        this.expiryDate = expiryDate;
    }

    @Column(name = "is_active")
    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("createDate", createDate)
                .omitNullValues()
                .toString();
    }


}
