package com.beans.model.user;

import com.beans.model.base.BaseModel;

import java.time.LocalDateTime;
import java.util.Map;

public class UserSession extends BaseModel {
    private static final long serialVersionUID = -2309150673435680771L;

    private User                    user = null;
    private	String	                token;

    public UserSession() {}
    public UserSession(User user, String token){
        this.user = user;
        this.token = token;
    }

    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }

}
