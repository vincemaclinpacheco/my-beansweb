package com.beans.model.publisher;

import com.beans.model.base.BaseModel;

public class PublisherDto extends BaseModel {

    private static final long serialVersionUID = -3912567123299461307L;

    private String          name;
    private String          city;
    private String          country;
    private String          email;
    private String          telephone;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
}
