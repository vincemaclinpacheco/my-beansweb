package com.beans.model.doctor;



import com.beans.model.base.BaseModel;

import javax.persistence.*;

@Entity
public class Doctor extends BaseModel {

    private String name;
    private String specialist;

    protected Doctor(){}

    public Doctor(String name, String specialist) {
        this.name = name;
        this.specialist = specialist;
    }


    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "specialist")
    public String getSpecialist() {
        return specialist;
    }

    public void setSpecialist(String specialist) {
        this.specialist = specialist;
    }
}
