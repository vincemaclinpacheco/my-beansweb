package com.beans.model.book;

import com.beans.model.author.Author;
import com.beans.model.author.AuthorDto;
import com.beans.model.base.BaseModel;
import com.beans.model.publisher.PublisherDto;

import java.time.LocalDate;

public class BookDto extends BaseModel {
    private static final long serialVersionUID = -3962512063254461307L;

    private String          title;
    private String          format;
    private Double          price;
    private LocalDate       publishDate;

    private AuthorDto       author;
    private PublisherDto    publisher;


    public AuthorDto getAuthor() {
        return author;
    }

    public void setAuthor(AuthorDto author) {
        this.author = author;
    }

    public PublisherDto getPublisher() {
        return publisher;
    }

    public void setPublisher(PublisherDto publisher) {
        this.publisher = publisher;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public LocalDate getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(LocalDate publishDate) {
        this.publishDate = publishDate;
    }


}
