package com.beans.model.book;


import com.beans.model.author.Author;
import com.beans.model.base.BaseModel;
import com.beans.model.publisher.Publisher;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.base.MoreObjects;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "t_beans_book")
public class Book extends BaseModel {

    private static final long serialVersionUID = -3962567063254461307L;

    private String          title;
    private String          format;
    private Double          price;
    private LocalDate       publishDate;
//    private Long            authorDbId;
//    private Long            publisherDbId;

    @JsonIgnoreProperties("bookSet")
    private Author          author;

    @JsonIgnoreProperties("bookSet")
    private Publisher       publisher;

    public Book() {}

    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    @JoinColumn(name = "author_db_id", referencedColumnName = "db_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    public Author getAuthor(){ return author; }
    public void setAuthor(Author author) { this.author = author; }

    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    @JoinColumn(name = "publisher_db_id", referencedColumnName = "db_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    public Publisher getPublisher(){ return publisher; }
    public void setPublisher(Publisher publisher) { this.publisher = publisher; }

    @Column(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "format")
    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    @Column(name = "price")
    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Column(name = "publish_date")
    public LocalDate getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(LocalDate publishDate) {
        this.publishDate = publishDate;
    }

//    @Column(name = "author_db_id")
//    public Long getAuthorDbId() {
//        return authorDbId;
//    }
//
//    public void setAuthorDbId(Long authorDbId) {
//        this.authorDbId = authorDbId;
//    }

//    @Column(name = "publisher_db_id")
//    public Long getPublisherDbId() {
//        return publisherDbId;
//    }
//
//    public void setPublisherDbId(Long publisherDbId) {
//        this.publisherDbId = publisherDbId;
//    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("title", title)
                .add("format", format)
                .omitNullValues()
                .toString();
    }
}
