package com.beans.model.base.query;

import com.google.common.base.Joiner;
import com.google.common.base.MoreObjects;

import java.io.Serializable;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public class SortOrder implements Serializable {
    private static final long serialVersionUID = 5892186496747906937L;
    private final String property;
    private final Direction direction;
    public static final Direction DFLT_DIRECTION = Direction.ASC;

    public enum Direction {
        ASC	("ASC"),
        DESC("DESC");
        //Lookup to allow retrieval of Status value using its corresponding String value.
        private static final Map<String, Direction> lookup = new HashMap<String,Direction>();
        static {
            for(Direction type : EnumSet.allOf(Direction.class)) {
                lookup.put(type.getValue(), type);
            }
        }

        private final String value;
        private Direction(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
        //Retrieve Status via its String equivalent
        public static Direction get(String type) {
            return lookup.get(type);
        }
    }

    public SortOrder(String property) {
        this(DFLT_DIRECTION, property);
    }
    public SortOrder(Direction direction, String property) {
        this.direction = direction == null ? DFLT_DIRECTION : direction;
        this.property = property;
    }

    public String getProperty() {
        return property;
    }
    public Direction getDirection() {
        return direction;
    }
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .addValue(
                        Joiner.on(" ").join("by:", getProperty(), getDirection().getValue())
                ).toString();
    }
}
