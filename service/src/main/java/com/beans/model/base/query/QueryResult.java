package com.beans.model.base.query;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class QueryResult<Entity> implements Serializable {
    private static final long serialVersionUID = 7149844209474324783L;
    private Boolean success;
    private Integer numberOfElements;
    private Long totalElements;
    private Integer totalPages;
    private List<Entity> contentList;
    private String errorMessage;

    // added some default values to the default constructor
    public QueryResult() {
        this.success = Boolean.TRUE;
        this.numberOfElements = 1;
        this.totalElements = 1L;
        this.totalPages = 1;
        this.contentList = new ArrayList<>();
        this.errorMessage = "";
    }

    public Boolean getSuccess() {
        return success;
    }
    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getNumberOfElements() {
        return numberOfElements;
    }
    public void setNumberOfElements(Integer numberOfElements) {
        this.numberOfElements = numberOfElements;
    }

    public Long getTotalElements() {
        return totalElements;
    }
    public void setTotalElements(Long totalElements) {
        this.totalElements = totalElements;
    }

    public Integer getTotalPages() {
        return totalPages;
    }
    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public List<Entity> getContentList() {
        return contentList;
    }
    public void setContentList(List<Entity> contentList) {
        this.contentList = contentList;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @JsonIgnore
    public Entity getContent() {
        return hasContent() ? contentList.get(0) : null;
    }
    @JsonIgnore
    public void setContent(Entity entity) {
        if (!hasContent()) setContentList(new ArrayList<>());
        getContentList().add(entity);
    }
    @JsonIgnore
    public boolean hasContent() {
        return getContentList() != null && !getContentList().isEmpty();
    }
}
