package com.beans.model.base.query;

import com.google.common.base.MoreObjects;
import com.querydsl.core.types.Ops;

import java.util.HashMap;
import java.util.Map;

public enum QueryOperator {
    BTWN(Ops.BETWEEN),
    EQ(Ops.EQ),
    EQ_IC(Ops.EQ_IGNORE_CASE),
    NE(Ops.NE),
    GT(Ops.GT),
    GTE(Ops.GOE),
    LT(Ops.LT),
    LTE(Ops.LOE),
    IN(Ops.IN),
    LIKE(Ops.LIKE);

    private static final Map<String, QueryOperator> opMap = new HashMap<>();
    private final Ops operand;

    QueryOperator(Ops operand) {
        this.operand = operand;
    }

    public Ops getOperand() {
        return this.operand;
    }
    public static Map<String, QueryOperator> getOpMap() {
        return opMap;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("operator", operand.toString())
                .toString();
    }
}
