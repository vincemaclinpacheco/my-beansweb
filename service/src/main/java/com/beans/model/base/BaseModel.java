package com.beans.model.base;


import javax.persistence.*;
import java.io.Serializable;

@MappedSuperclass
public class BaseModel implements Serializable {
    private static final long serialVersionUID = -8530740835730664250L;
    protected Long id;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "db_id")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Transient
    public String getClassName() {
        String name = this.getClass().getName();
        return name.substring(name.lastIndexOf(".") + 1);
    }

    @Override
    public boolean equals (Object object) {
        if (this == object) return true;
        if (this.id == null || object == null || !(this.getClass().equals(object.getClass()))) return false;
        BaseModel toCompare = (BaseModel) object;
        return this.id.equals(toCompare.getId());
    }

    @Override
    public int hashCode() {
        return id == null ? 0 : id.hashCode();
    }
}
