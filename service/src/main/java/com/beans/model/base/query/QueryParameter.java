package com.beans.model.base.query;

import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class QueryParameter implements Serializable {
    private static final long serialVersionUID = 8470786717664587189L;

    private String property;
    private com.beans.model.base.query.QueryOperator qOperator;
    private Object value;
    private Boolean isNegated;
    private List<QueryParameter> orOperands;

    public QueryParameter(){
    }
    public QueryParameter(String property, String opReference, Object value) {
        this.property = property;
        this.qOperator = com.beans.model.base.query.QueryOperator.getOpMap().get(opReference);
        this.value = value;
    }
    public QueryParameter(String property, String opReference, Object value, Boolean isNegated) {
        this(property, opReference, value);
        this.isNegated = isNegated;
    }
    public QueryParameter(String property, com.beans.model.base.query.QueryOperator qOperator, Object value) {
        this.property = property;
        this.qOperator = qOperator;
        this.value = value;
    }
    public QueryParameter(String property, com.beans.model.base.query.QueryOperator qOperator, Object value, Boolean isNegated) {
        this(property, qOperator, value);
        this.isNegated = isNegated;
    }

    public String getProperty() {
        return property;
    }
    public void setProperty(String property) {
        this.property = property;
    }

    public com.beans.model.base.query.QueryOperator getqOperator() {
        return qOperator;
    }
    public void setqOperator(com.beans.model.base.query.QueryOperator qOperator) {
        this.qOperator = qOperator;
    }

    public Object getValue() {
        return value;
    }
    public void setValue(Object value) {
        this.value = value;
    }

    public Boolean getNegated() {
        return isNegated;
    }
    public void setNegated(Boolean negated) {
        isNegated = negated;
    }

    public List<QueryParameter> getOrOperands() {
        return orOperands;
    }
    public void setOrOperands(List<QueryParameter> orOperands) {
        this.orOperands = orOperands;
    }
    public void setOrOperandsFromArr(com.beans.model.base.query.QueryParameter... orOperandsFromArr) {
        this.orOperands = Lists.newArrayList(orOperandsFromArr);
    }
    public boolean hasOrOperands() {
        return getOrOperands() != null && !getOrOperands().isEmpty();
    }
    public void appendOrOperand(com.beans.model.base.query.QueryParameter orOperand) {
        if(!hasOrOperands()) setOrOperands(new ArrayList<>());
        getOrOperands().add(orOperand);
    }

    @Override
    public String toString() {
        return "[Property:" + getProperty() + " Operator:" + getqOperator().toString() + " Value:" + getValue().toString() + " Negated:" + getNegated() + "]";
    }
}
