package com.beans.service.publisher;

import com.beans.model.publisher.Publisher;
import com.beans.service.base.BaseServiceImpl;
import org.springframework.stereotype.Service;


@Service("publisherService")
public class PublisherServiceImpl extends BaseServiceImpl<Publisher, Long> implements PublisherService {
}
