package com.beans.service.publisher;

import com.beans.model.publisher.Publisher;
import com.beans.service.base.BaseService;

public interface PublisherService extends BaseService<Publisher, Long> {
}
