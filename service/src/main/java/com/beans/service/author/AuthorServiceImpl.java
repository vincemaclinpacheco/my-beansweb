package com.beans.service.author;


import com.beans.model.author.Author;
import com.beans.service.base.BaseServiceImpl;
import org.springframework.stereotype.Service;

@Service("authorService")
public class AuthorServiceImpl extends BaseServiceImpl<Author, Long> implements AuthorService {
}
