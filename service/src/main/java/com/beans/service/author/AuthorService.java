package com.beans.service.author;

import com.beans.model.author.Author;
import com.beans.service.base.BaseService;

public interface AuthorService  extends BaseService<Author, Long> {
}
