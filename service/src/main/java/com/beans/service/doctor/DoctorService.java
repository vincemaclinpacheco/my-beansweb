package com.beans.service.doctor;

import com.beans.model.doctor.Doctor;
import com.beans.service.base.BaseService;

import java.util.List;

public interface DoctorService extends BaseService<Doctor, Long> {

    List<Doctor> getDoctors();
}
