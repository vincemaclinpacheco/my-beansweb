package com.beans.service.doctor;

import com.beans.model.doctor.Doctor;
import com.beans.repository.doctor.DoctorRepository;
import com.beans.service.base.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service("doctorService")
public class DoctorServiceImpl extends BaseServiceImpl<Doctor, Long> implements DoctorService {

    @Autowired
    private DoctorRepository repository;

    @Override
    public List<Doctor> getDoctors() {
        return (List<Doctor>) repository.findAll();
    }
}
