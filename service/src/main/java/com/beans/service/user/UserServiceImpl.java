package com.beans.service.user;

import com.beans.model.base.query.QueryOperator;
import com.beans.model.base.query.QueryParameter;
import com.beans.model.user.PasswordHistory;
import com.beans.model.user.User;
import com.beans.repository.user.UserRepository;
import com.beans.service.base.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@Service("userService")
public class UserServiceImpl extends BaseServiceImpl<User, Long> implements UserService {

    @Autowired
    UserRepository userRepository;

    @Resource
    private PasswordHistoryService passwordService;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    @Override
    public User create(User user) {
        if (checkUserId(user)) {
            user = super.create(user);
        }
        if (createPassword(user)) {
            return user;
        } else {
            super.delete(user.getId());
            return null;
        }
    }

    @Override
    public List<User> listUsers() {
        return (List<User>) userRepository.findAll();
    }


    private boolean checkUserId(User newUser) {
        User user = super.retrieve(
                new QueryParameter("userId", QueryOperator.EQ_IC, newUser.getUserId())
        );

        if (user != null) {
            return false;
        } else {
            return true;
        }
    }

    private boolean createPassword(User newUser) {
        User user = super.retrieve(
                new QueryParameter("userId", QueryOperator.EQ_IC, newUser.getUserId())
        );
        PasswordHistory newActivePassword = new PasswordHistory();
        LocalDate currentDate = LocalDate.now();

        newActivePassword.setCreateDate(currentDate);
        newActivePassword.setExpiryDate(currentDate);
        newActivePassword.setPassword(bcryptEncoder.encode(user.getPassword()));
        newActivePassword.setUserDbId(user.getId());
        newActivePassword.setIsActive(1);
        passwordService.create(newActivePassword);

        return true;
    }


}
