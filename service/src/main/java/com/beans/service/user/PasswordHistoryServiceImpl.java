package com.beans.service.user;

import com.beans.model.base.query.QueryOperator;
import com.beans.model.base.query.QueryParameter;
import com.beans.model.base.query.SortOrder;
import com.beans.model.user.PasswordHistory;
import com.beans.model.user.User;
import com.beans.repository.user.PasswordHistoryRepository;
import com.beans.service.base.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service("passwordHistoryService")
public class PasswordHistoryServiceImpl extends BaseServiceImpl<PasswordHistory, Long> implements PasswordHistoryService {

    @Resource
    private PasswordHistoryRepository repository;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    @Override
    public boolean updatePassword(User user, String newPassword) {

        try {
            LocalDate currentDate = LocalDate.now();
            LocalDate passwordReuseLimit = currentDate.minusDays(90);

            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

            List<QueryParameter> parameterList = new ArrayList<>();
            List<QueryParameter> parameterList1 = new ArrayList<>();
            parameterList.add(new QueryParameter("userDbId", QueryOperator.EQ, user.getId()));
            parameterList.add(new QueryParameter("createDate", QueryOperator.GTE, passwordReuseLimit));
            parameterList.add(new QueryParameter("createDate", QueryOperator.LTE, currentDate));

            SortOrder sortByIsActive = new SortOrder(SortOrder.Direction.DESC, "isActive");

            List <PasswordHistory> unusableList = listAll(parameterList, sortByIsActive).getContentList();
            for (PasswordHistory pastPassword : unusableList) {
                if (encoder.matches(newPassword, pastPassword.getPassword())) { // added for encrypted password
                    return false;
                } else if (pastPassword.getPassword().equals(newPassword)) {
                    return false;
                }
            }

            if (unusableList.isEmpty()) {
                parameterList1.add(new QueryParameter("userDbId", QueryOperator.EQ, user.getId()));
                parameterList1.add(new QueryParameter("createDate", QueryOperator.LTE, currentDate));
                List <PasswordHistory> passwordList = listAll(parameterList1, sortByIsActive).getContentList();
                PasswordHistory currentPassword = passwordList.get(0);
                currentPassword.setIsActive(0);
                this.update(currentPassword);
            } else {
                PasswordHistory currentPassword = unusableList.get(0);
                currentPassword.setIsActive(0);
                this.update(currentPassword);
            }

            // create a new PasswordHistory entity and add as the new active password
            PasswordHistory newActivePassword = new PasswordHistory();
            LocalDate newExpiryDate = currentDate.plusDays(90);
            newActivePassword.setCreateDate(currentDate);
            newActivePassword.setExpiryDate(newExpiryDate);
            newActivePassword.setPassword(bcryptEncoder.encode(newPassword));
            newActivePassword.setUserDbId(user.getId());
            newActivePassword.setIsActive(1);
            this.create(newActivePassword);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean resetPassword(User user, String newPassword) {
        try {
            LocalDate currentDate = LocalDate.now();
            LocalDate passwordReuseLimit = currentDate.minusDays(90);

            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

            List<QueryParameter> parameterList = new ArrayList<>();
            List<QueryParameter> parameterList1 = new ArrayList<>();
            parameterList.add(new QueryParameter("user.id", QueryOperator.EQ, user.getId()));
            parameterList.add(new QueryParameter("createDate", QueryOperator.GTE, passwordReuseLimit));
            parameterList.add(new QueryParameter("createDate", QueryOperator.LTE, currentDate));
            SortOrder sortByIsActive = new SortOrder(SortOrder.Direction.DESC, "isActive");

            List <PasswordHistory> unusableList = listAll(parameterList, sortByIsActive).getContentList();
            for (PasswordHistory pastPassword : unusableList) {
                if (encoder.matches(newPassword, pastPassword.getPassword())) { // added for encrypted password
                    return false;
                } else if (pastPassword.getPassword().equals(newPassword)) {
                    return false;
                }
            }

            if (unusableList.isEmpty()) {
                parameterList1.add(new QueryParameter("user.id", QueryOperator.EQ, user.getId()));
                parameterList1.add(new QueryParameter("createDate", QueryOperator.LTE, currentDate));
                List <PasswordHistory> passwordList = listAll(parameterList1, sortByIsActive).getContentList();
                PasswordHistory currentPassword = passwordList.get(0);
                currentPassword.setIsActive(0);
                this.update(currentPassword);
            } else {
                PasswordHistory currentPassword = unusableList.get(0);
                currentPassword.setIsActive(0);
                this.update(currentPassword);
            }


            // create a new PasswordHistory entity and add as the new active password
            PasswordHistory temporaryPassword = new PasswordHistory();
            temporaryPassword.setCreateDate(currentDate);
            temporaryPassword.setExpiryDate(currentDate);
            temporaryPassword.setPassword(bcryptEncoder.encode(newPassword));
            temporaryPassword.setUserDbId(user.getId());
            temporaryPassword.setIsActive(1);
            this.create(temporaryPassword);
            return true;
        } catch(Exception e) {
            return false;
        }
    }
}
