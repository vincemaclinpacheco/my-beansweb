package com.beans.service.user;

import com.beans.model.user.PasswordHistory;
import com.beans.model.user.User;
import com.beans.service.base.BaseService;

public interface PasswordHistoryService extends BaseService<PasswordHistory, Long> {
    boolean updatePassword(User user, String newPassword);
    boolean resetPassword(User user, String newPassword);
}
