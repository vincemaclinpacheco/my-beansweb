package com.beans.service.user;

import com.beans.model.user.User;
import com.beans.service.base.BaseService;

import java.util.List;

public interface UserService extends BaseService<User, Long> {

    List<User> listUsers();
}
