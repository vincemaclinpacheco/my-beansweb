package com.beans.service.book;

import com.beans.model.book.Book;
import com.beans.service.base.BaseService;

public interface BookService extends BaseService<Book, Long> {
}
