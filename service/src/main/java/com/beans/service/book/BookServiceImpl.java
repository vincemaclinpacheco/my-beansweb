package com.beans.service.book;

import com.beans.model.book.Book;
import com.beans.service.base.BaseServiceImpl;
import org.springframework.stereotype.Service;

@Service("bookService")
public class BookServiceImpl extends BaseServiceImpl<Book, Long> implements BookService {
}
