package com.beans.service.seeder;

import com.beans.model.base.query.QueryOperator;
import com.beans.model.base.query.QueryParameter;
import com.beans.model.base.query.SortOrder;
import com.beans.model.doctor.Doctor;
import com.beans.model.user.PasswordHistory;
import com.beans.model.user.User;
import com.beans.repository.doctor.DoctorRepository;
import com.beans.repository.user.UserRepository;
import com.beans.service.user.PasswordHistoryService;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Service
public class DbSeeder {

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private UserRepository userRepository;




    @PostConstruct
    public void initDoctor() {
        doctorRepository.saveAll(Stream.of(new Doctor( "John", "Cardiac"),
                new Doctor("Peter", "eye"))
                .collect(Collectors.toList()));
    }

    @PostConstruct
    public void initUsers() {
//        userRepository.saveAll(Stream.of(new User("USR00", "Vince", "Pacheco", "vincemaclinpacheco@gmail.com", "admin"))
//                .collect(Collectors.toList()));
    }

}
