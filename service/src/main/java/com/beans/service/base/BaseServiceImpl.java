package com.beans.service.base;

import com.beans.model.base.query.QueryOperator;
import com.beans.model.base.query.QueryParameter;
import com.beans.model.base.query.QueryResult;
import com.beans.model.base.query.SortOrder;
import com.beans.repository.base.BaseRepository;
import com.google.common.collect.Iterables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.util.List;

public class BaseServiceImpl<Entity, ID extends Serializable> implements BaseService<Entity, ID> {

    @PersistenceContext
    protected EntityManager entityManager;

    @Autowired
    private BaseRepository<Entity, ID> repository;
    protected Class<Entity> entityClass;

    public BaseServiceImpl() {}

    @Transactional(propagation = Propagation.REQUIRED)
    public Entity create (Entity entity) {
        return repository.save(entity);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public <S extends Entity> Iterable<S> create (Iterable<S> entities) {
        return repository.saveAll(entities);
    }

    @Transactional(readOnly = true)
    public Entity retrieve (QueryParameter... parameters) {
        return repository.retrieve(parameters);
    }

    @Transactional(readOnly = true)
    public Entity retrieve (EntityGraph<Entity> graph, QueryParameter ... parameters) {
        return repository.retrieve(graph, parameters);
    }


    @Transactional(readOnly = true)
    public Entity retrieve (ID id) {
        return repository.retrieve(id);
    }

    @Transactional(readOnly = true)
    public Entity retrieve (EntityGraph<Entity> graph, ID id) {
        return repository.retrieve(graph, new QueryParameter("id", QueryOperator.EQ, id));
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Entity update (Entity entity) {
        return repository.save(entity);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public <S extends Entity> Iterable<S> update (Iterable<S> entities) {
        return repository.saveAll(entities);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void delete (ID id) {
        repository.delete(id);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void delete (QueryParameter ... parameters) {
        repository.delete(parameters);
    }

    @Transactional(readOnly = true)
    public QueryResult<Entity> listAll() {
        return repository.listAll();
    }

    @Transactional(readOnly = true)
    public QueryResult<Entity> listAll(List<QueryParameter> parameterList, SortOrder... sortOrders) {
        return repository.listAll(parameterList, sortOrders);
    }

    @Transactional(readOnly = true)
    public QueryResult<Entity> listAll(List<QueryParameter> parameterList, int pageSize, int pageStart, SortOrder ... sortOrders) {
        return repository.listAll(parameterList, pageSize, pageStart, sortOrders);
    }

    @Transactional(readOnly = true)
    public QueryResult<Entity> listAll(EntityGraph<Entity> graph, List <QueryParameter> parameterList, int pageStart, int pageSize, SortOrder ... sortOrders){
        return repository.listAll(graph, parameterList, pageStart, pageSize, sortOrders);
    }

    @Override
    public int count(List<QueryParameter> parameterList) {
        return (int) repository.count(Iterables.toArray(parameterList, QueryParameter.class));
    }

    @Override
    public int count(QueryParameter... parameters) {
        return (int) repository.count(parameters);
    }

    @Override
    public boolean exists(QueryParameter... parameters) {
        return repository.exists(parameters);
    }

    @Override
    public boolean exists(List<QueryParameter> parameterList) {
        return repository.exists(Iterables.toArray(parameterList, QueryParameter.class));
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public BaseRepository<Entity, ID> getRepository() {
        return repository;
    }
    public void setRepository(BaseRepository<Entity, ID> repository) {
        this.repository = repository;
    }
}
