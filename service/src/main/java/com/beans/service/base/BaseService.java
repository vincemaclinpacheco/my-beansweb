package com.beans.service.base;

import com.beans.model.base.query.QueryParameter;
import com.beans.model.base.query.QueryResult;
import com.beans.model.base.query.SortOrder;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import java.util.List;

public interface BaseService<Entity, ID> {

    EntityManager getEntityManager();

    // CREATE
    Entity create (Entity entity);
    <S extends Entity> Iterable <S> create (Iterable <S> entitites);

    // RETRIEVE
    Entity retrieve (ID id);
    Entity retrieve (EntityGraph<Entity> graph, ID id);
    Entity retrieve (QueryParameter... parameters);
    Entity retrieve (EntityGraph<Entity> graph, QueryParameter... parameters);

    // UPDATE
    Entity update (Entity entity);
    <S extends Entity> Iterable<S> update (Iterable<S> entities);

    // DELETE
    void delete(ID id);
    void delete(QueryParameter ... parameters);

    // LIST ALL
    QueryResult<Entity> listAll();
    QueryResult<Entity> listAll(List<QueryParameter> parameterList, SortOrder... sortOrders);
    QueryResult<Entity> listAll(List<QueryParameter> parameterList, int pageStart, int pageSize, SortOrder ... sortOrders);
    QueryResult<Entity> listAll(EntityGraph<Entity> graph, List<QueryParameter> parameterList, int pageStart, int pageSize, SortOrder ... sortOrders);

    // Miscellaneous methods
    int count(List<QueryParameter> parameterList);
    int count(QueryParameter ... parameters);
    boolean exists (QueryParameter ... parameters);
    boolean exists (List<QueryParameter> parameterList);
}
